﻿**Experis Academy, Norway**

**Authors:**
* **Ørjan Storås**

# Experis Week4 Task 27
## Task 27

- [x] Create a Web Api from an empty ASP.NET Core Application which allows for clients to get all the current supervisors in a DB as JSON
- [x] It must use Entity Framework code first
- [x] The supervisor class must only have a maximum of two properties (It should also have an Id) Therefore, total of
three